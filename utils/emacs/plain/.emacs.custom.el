(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default-frame-alist '((vertical-scroll-bars)))
 '(exec-path
   '("c:/Windows/system32" "C:/Windows" "C:/Windows/System32/Wbem" "C:/Windows/System32/WindowsPowerShell/v1.0/" "C:/Windows/System32/OpenSSH/" "C:/Program Files/Neovim/bin" "C:/Program Files (x86)/NVIDIA Corporation/PhysX/Common" "C:/Program Files/NVIDIA Corporation/NVIDIA NvDLISR" "C:/Program Files/starship/bin/" "C:/Program Files (x86)/Windows Kits/10/Windows Performance Toolkit/" "C:/Program Files/dotnet/" "C:/Program Files (x86)/ePapyrus/Papyrus-PlugIn-web" "C:/Program Files (x86)/ePapyrus/Papyrus-PlugIn-web/Addins" "C:/Program Files/Go/bin" "C:/WINDOWS/system32" "C:/WINDOWS" "C:/WINDOWS/System32/Wbem" "C:/WINDOWS/System32/WindowsPowerShell/v1.0/" "C:/WINDOWS/System32/OpenSSH/" "C:/Program Files/nodejs/" "C:/Program Files/Git/cmd" "C:/Program Files/Wolfram Research/WolframScript/" "c:/Program Files/Emacs/emacs-29.4/libexec/emacs/29.4/x86_64-w64-mingw32" "C:/Users/almag/.cargo/bin" "C:/Users/almag/.local/bin" "C:/Program Files/Microsoft Visual Studio/2022/Community/Common7/IDE/CommonExtensions/Microsoft/CMake/CMake/bin" nil))
 '(mc/always-run-for-all t)
 '(package-selected-packages
   '(org-make-toc org-auto-tangle magit zig-mode cdlatex pdf-tools multiple-cursors aas yasnippet smex which-key timu-caribbean-theme ivy))
 '(tab-always-indent 'complete)
 '(tab-width 4)
 '(truncate-lines nil)
 '(warning-minimum-level :error)
 '(word-wrap nil))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

