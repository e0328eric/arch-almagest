# Arch Linux Setting Files

# Screen Resolution Setting
If ones screen scaling is too small, then add these lines into `~/.Xresources`.
```text
Xft.dpi:        156
Xft.antialias:  true
Xft.hinting:    true
Xft.rgba:       rgb
Xft.hintstyle:  hintslight
```

# lightdm config
I used [Shikai](https://github.com/TheWisker/Shikai) theme
