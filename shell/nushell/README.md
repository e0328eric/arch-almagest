# Setting Theme Configuration

First install `oh-my-posh` described in [here](https://ohmyposh.dev).
Then run the following command:

```console
$ oh-my-posh init nu --config night-owl.omp.toml
```
