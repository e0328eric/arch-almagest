return {
	keybindings = require("settings.keybindings"),
	rules = require("settings.rules"),
	statusbar = require("settings.statusbar"),
}
