export GTK_IM_MODULE=fcitx # fcitx5 might show a warning if enable, but should be enabled for Chrome.
export QT_IM_MODULE=fcitx
export XMODIFIERS=@im=fcitx
export GLFW_IM_MODULE=ibus
export SDL_IM_MODULE=fcitx
export QT_QPA_PLATFORMTHEME=qt5ct
